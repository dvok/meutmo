﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using System.Collections.Generic;
using Vectrosity;
using System;

public class TestDerivative : MonoBehaviour {

	private List<Vector3> dwPoints = new List<Vector3>();
	private Current curr;
	private MemristorTEAM model;

	private TEAMParameters p;

	private double w;

	double Timer = 0d;

	void Start () {
		p = new TEAMParameters ();
		w = 0.5 * 3e-9d;
		curr = new Current (-1.2e-4d);
		model = new MemristorTEAM (Polarity.positive);
	}

	void FixedUpdate () {
//		curr.Value += 1e-6d /10;

		Timer += Time.fixedTime;
		curr.Value = 0.003 * System.Math.Sin (2e6 * 2 * System.Math.PI * Timer);

		double dw = 0d;

		if (curr.Value > 0 && curr.Value > p.I_off.Value) {
			dw = p.K_off * Math.Pow (curr.Value / p.I_off.Value - 1, p.Alpha_off) * p.F_off (w);
		}

		if (curr.Value <= 0 && curr.Value < p.I_on.Value) {
			dw = p.K_on * Math.Pow (curr.Value / p.I_on.Value - 1, p.Alpha_on) * p.F_on (w);
		}

		w += dw * Time.fixedDeltaTime;

		dwPoints.Add (new Vector3 ((float)curr.Value * 1e4f, (float)dw * 1e9f));

		Debug.Log ("I = "+(float)curr.Value * 1e4f + " dW = " + (float)dw * 1e9f);
		Debug.Log ("w = " + w + " f_off = " + p.F_off (w) + " f_on = " + p.F_on (w));



	}

	void LateUpdate () {
	}

	// Update is called once per frame
	void Update () {
//		if (curr.Value > 12e-4d) {
			VectorLine dwGraph = new VectorLine ("dwGraph", dwPoints, null, 1f, LineType.Continuous);
			dwGraph.Draw3D ();
//			Debug.Break ();
//		}
	}
}
