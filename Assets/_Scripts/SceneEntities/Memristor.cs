﻿using UnityEngine;
using UnityEditor;
using AssemblyCSharp;
using System.Collections.Generic;
using Vectrosity;
using System;

public class Memristor : MonoBehaviour {

	public MemristorTEAM MemristorModel; 
	public double Frequency = 2000000d;
	public double Amp = 0.003d;
	public double Timer = 0.0d;
	public double TimeLimit = 100d;
	public double TimeStep = 0.02d;
	public Current curr = new Current(0d);
	double w;
	TEAMParameters p;

	private int i = 0;
	private List<Vector3> inputCurrentGraph = new List<Vector3> ();
	private List<Vector3> memristanceGraph = new List<Vector3> ();
	private List<Vector3> internalStateGraph = new List<Vector3> ();
	private List<Vector3> voltageStateGraph = new List<Vector3> ();
	private List<Vector3> ivGraph = new List<Vector3> ();
	private List<Vector3> dwIGraph = new List<Vector3> ();




	// Use this for initialization
	void Start () {
		MemristorModel = new MemristorTEAM (Polarity.positive);
		w = 0.5d * 3e-9d;
		p = new TEAMParameters ();
		Timer = 0;


	}

	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		if (Timer < TimeLimit) {
			

			curr.Value = Amp * System.Math.Sin (Frequency * 2 * System.Math.PI * Timer);
//			MemristorModel.Input (new Current (Current.Value));

//
//			memristanceGraph.Add (new Vector3 ((float)Timer , (float)MemristorModel.Memristance.Value / 10f, 0f));
			inputCurrentGraph.Add (new Vector3 ((float)Timer , (float)curr.Value * 1e4f, -5f));
//			internalStateGraph.Add (new Vector3 ((float)Timer, (float)MemristorModel.InternalState* 10000000000f, +5f)); 
//			voltageStateGraph.Add (new Vector3 ((float)Timer, (float)(Current.ToVoltage (MemristorModel.Memristance)).Value, 10f));
//			ivGraph.Add(new Vector3((float)(Current.ToVoltage (MemristorModel.Memristance)).Value*5f, (float)Current.Value* 10000f, 0f));

			double dw = 0d;

			if (curr.Value > 0 && curr.Value > p.I_off.Value) {
				dw = p.K_off * Math.Pow (curr.Value / p.I_off.Value - 1, p.Alpha_off) * p.F_off (w);
			}

			if (curr.Value <= 0 && curr.Value < p.I_on.Value) {
				dw = p.K_on * Math.Pow (curr.Value / p.I_on.Value - 1, p.Alpha_on) * p.F_on (w);
			}

			w += dw * Time.fixedDeltaTime;
			Timer += Time.fixedDeltaTime;

			dwIGraph.Add (new Vector3((float)curr.Value * 1e4f, (float) dw * 1e9f));


			Debug.Log("I: " + curr.Value* 1e4f + " // dW: " + dw* 1e9f);

//			Debug.Log ("Curr: " + Current.Value + " // Voltage: " + Current.ToVoltage (MemristorModel.Memristance).Value);
//			Debug.Log("Memristance: " + MemristorModel.Memristance.Value + " // InternalState: " + MemristorModel.InternalState);
			i++;
		}

		if (i > 1) {

			VectorLine.SetLine3D (Color.red, dwIGraph[i-2], dwIGraph[i-1]);

			VectorLine.SetLine3D (Color.green, inputCurrentGraph[i-2], inputCurrentGraph[i-1]);
//			VectorLine.SetLine3D (Color.red, memristanceGraph[i-2], memristanceGraph[i-1]);
//			VectorLine.SetLine3D (Color.cyan, internalStateGraph[i-2], internalStateGraph[i-1]);
//			VectorLine.SetLine3D (Color.yellow, voltageStateGraph[i-2], voltageStateGraph[i-1]);
//			VectorLine.SetLine3D (Color.white, ivGraph[i-2], ivGraph[i-1]);



		}

		if (Timer >= TimeLimit) {
			

			Debug.Break ();
		}

	}


}
