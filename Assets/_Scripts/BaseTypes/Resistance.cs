﻿using System;

namespace AssemblyCSharp
{
	public class Resistance
	{
		public double Value {
			get;
			set;
		}
		#region conversion
		public double ToDouble(){
			return Value;
		}

		public Current ToCurrent(Voltage V) {
			return new Current (Value / V.Value);
		}

		public Voltage ToVoltage(Current I) {
			return new Voltage (Value * I.Value);
		}
		#endregion

		#region constructors
		public Resistance (double value)
		{
			Value = value;
		}
		#endregion

		#region operators
		public static Resistance operator + (Resistance a, Resistance b) {
			return new Resistance (a.Value + b.Value);
		}

		public static Resistance operator - (Resistance a, Resistance b) {
			return new Resistance (a.Value - b.Value);
		}

		public static bool operator == (Resistance a, Resistance b) {
			return Math.Abs (a.Value - b.Value) < Constants.EPSILON;
		}

		public static bool operator != (Resistance a, Resistance b) {
			return Math.Abs (a.Value - b.Value) > Constants.EPSILON;
		}
		#endregion
	}
}

