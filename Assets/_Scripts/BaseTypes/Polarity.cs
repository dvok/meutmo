﻿using System;

namespace AssemblyCSharp
{
	public enum Polarity: int
	{
		positive = 1,
		negative = -1
	}
}

