﻿using System;

namespace AssemblyCSharp
{
	public class Voltage
	{
		public double Value {
			get;
			set;
		}

		#region conversion
		public double ToDouble(){
			return Value;
		}

		public Current ToCurrent(Resistance R) {
			return new Current(Value/R.Value);
		}

		public Resistance ToResistance(Current I) {
			return new Resistance (Value / I.Value);
		}
		#endregion

		#region constructors
		public Voltage (double value)
		{
			Value = value;
		}
		#endregion

		#region operators
		#region operators
		public static Voltage operator + (Voltage a, Voltage b) {
			return new Voltage (a.Value + b.Value);
		}

		public static Voltage operator - (Voltage a, Voltage b) {
			return new Voltage (a.Value - b.Value);
		}

		public static bool operator == (Voltage a, Voltage b) {
			return Math.Abs (a.Value - b.Value) < Constants.EPSILON;
		}

		public static bool operator != (Voltage a, Voltage b) {
			return Math.Abs (a.Value - b.Value) > Constants.EPSILON;
		}
		#endregion
		#endregion
	}
}

