﻿using System;

namespace AssemblyCSharp
{
	public enum MemristorControlType: int
	{
		CurrentControlled = 0,
		VoltageControlled = 1
	}
}

