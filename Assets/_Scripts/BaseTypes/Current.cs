﻿using System;

namespace AssemblyCSharp
{
	public class Current
	{
		public double Value {
			get;
			set;
		}

		#region conversion
		public double ToDouble(){
			return Value;
		}

		public Voltage ToVoltage(Resistance R) {
			return new Voltage (Value * R.Value);
		}

		public Resistance ToResistance(Voltage V) {
			return new Resistance (V.Value / Value);
		}
		#endregion

		#region constructors
		public Current (double value)
		{
			Value = value;
		}
		#endregion

		#region operators
		public static Current operator + (Current a, Current b) {
			return new Current (a.Value + b.Value);
		}

		public static Current operator - (Current a, Current b) {
			return new Current (a.Value - b.Value);
		}

		public static bool operator == (Current a, Current b) {
			return Math.Abs (a.Value - b.Value) < Constants.EPSILON;
		}

		public static bool operator != (Current a, Current b) {
			return Math.Abs (a.Value - b.Value) > Constants.EPSILON;
		}
		#endregion
	}
}

