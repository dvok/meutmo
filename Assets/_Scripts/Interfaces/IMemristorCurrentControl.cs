﻿using System;

namespace AssemblyCSharp
{
	public interface IMemristorCurrentControl: IMemristor {

		Current Input {
			get;
			set;
		}

	}
}
