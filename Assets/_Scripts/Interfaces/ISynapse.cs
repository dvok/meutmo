﻿using System;

namespace AssemblyCSharp
{
	public interface ISynapse
	{
		
		double SynapticWeight {
			get;
		}

		void AttachToNeuron(INeuron neuron);
	}
}
