﻿using System;

namespace AssemblyCSharp
{
	public interface IMemristorVoltageControl: IMemristor {

		Voltage Input {
			get;
			set;
		}

	}
}
