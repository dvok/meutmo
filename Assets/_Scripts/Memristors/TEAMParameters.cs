﻿using System;
using System.Diagnostics;

namespace AssemblyCSharp
{
	public class TEAMParameters
	{

		#region constants
		public double K_off;	//nanometers per second
		public double K_on;		//nanometers per second
		public double A_off;	//constant
		public double A_on;		//constant

		public double Alpha_off;
		public double Alpha_on;
		#endregion

		#region current thresholds
		public Current I_off;
		public Current I_on;
		#endregion

		#region state variable bounds
		public double W_on;		//nanometers
		public double W_off;	//nanometers
		public double W_c;
		#endregion

		#region effective resistance at bounds
		public Resistance R_off;
		public Resistance R_on;
		public double Lambda {
			get { 
				Debug.Assert (Math.Abs (R_off.Value) > Constants.EPSILON && Math.Abs (R_on.Value) > Constants.EPSILON);
				return Math.Log (R_off.Value / R_on.Value); 
			}
		}
		#endregion

		#region window functions
		public double F_off(double w) {
			return Math.Exp (-Math.Exp ((w - A_off) / W_c));
		}

		public double F_on(double w) {
			return Math.Exp (-Math.Exp (-(w - A_on) / W_c));
		}
		#endregion

		#region default setting
		public void SMTBFitting()
		{
			A_on = 1.8E-09d;
			A_off = 1.2E-09d;

			Alpha_on = 10d;
			Alpha_off = 10d;
			
			K_on = -4.68E-13d;	
			K_off = 1.46E-9d;	

			I_on = new Current(8.9E-06d);  
			I_off = new Current(115E-06d);
			
			W_on = 3E-09d;
			W_off = 0d;

			W_c = 107E-12d;

			R_off = new Resistance(16000d);
			R_on = new Resistance(116d);
		}
		#endregion

		#region constructors
		public TEAMParameters() {
			SMTBFitting ();
		}
		#endregion
	}
}

