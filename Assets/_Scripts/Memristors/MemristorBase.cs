﻿using System;

namespace AssemblyCSharp
{
	public abstract class MemristorBase
	{
		#region public properties
		abstract public Resistance Memristance {
			get;
		}

		abstract public double InternalState {
			get;
		}

		abstract public MemristorControlType ControlType {
			get;
		}

		virtual public Polarity Polarity {
			get { return polarity; }
		}
		#endregion

		#region input methods
		abstract public void Input(Current i);
		abstract public void Input(Voltage v);
		#endregion

		#region private properties
		protected Polarity polarity;
		#endregion

		#region constructor
		protected MemristorBase (Polarity p)
		{
			polarity = p;
		}
		#endregion
	}
}

