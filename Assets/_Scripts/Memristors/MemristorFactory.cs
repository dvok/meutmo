﻿using System;

namespace AssemblyCSharp
{
	public static class MemristorFactory 
	{
		public static MemristorBase Instantiate<T> (Polarity p) where T: MemristorBase {

			if (typeof(T) == typeof(MemristorTEAM)) {
				return new MemristorTEAM (p);
			}
			return null;
		}


	}
}

