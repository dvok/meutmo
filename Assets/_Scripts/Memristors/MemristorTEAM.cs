﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class MemristorTEAM: MemristorBase
	{	
		public double dw;
			
		#region inherited
		public override double InternalState {
			get { return internalState;	}
		}

		public override Polarity Polarity {
			get { return polarity; }
		}
	
		public override Resistance Memristance {
			get { return memristance; }
		}

		public override MemristorControlType ControlType {
			get { return MemristorControlType.CurrentControlled; }
		}
	
		public override void Input(Current i) {
			
			inputCurrent = i;
			inputCurrent.Value = inputCurrent.Value * (double)polarity;

			dw = 0d;

			if (inputCurrent.Value > 0 && inputCurrent.Value  > p.I_off.Value) {
				dw = p.K_off * Math.Pow (inputCurrent.Value / p.I_off.Value - 1, p.Alpha_off ) * p.F_off (internalState);
			} else if (inputCurrent.Value <= 0 && inputCurrent.Value < p.I_on.Value) {
				dw = p.K_on * Math.Pow (inputCurrent.Value / p.I_on.Value - 1, p.Alpha_on  ) * p.F_on (internalState);
			}

			internalState += dw * Time.fixedDeltaTime*10f; 

			memristance = getMemristance ();

		}
			
		public override void Input(Voltage v) {
			throw new WrongInputException();
		}
		#endregion
		

		#region private
		private Current inputCurrent = new Current(0d);
		private Resistance memristance = new Resistance(0d);
		private double internalState = 0d;
		private readonly TEAMParameters p;
		private Resistance getMemristance ()
		{
			return new Resistance(p.R_on.Value * Math.Exp (p.Lambda / (p.W_off - p.W_on) * (internalState - p.W_on)));
		}
		#endregion


		#region constructors
		public MemristorTEAM (Polarity polarity, TEAMParameters parameters): base(polarity)
		{
			p = parameters;
			memristance = getMemristance ();
		}

		public MemristorTEAM (Polarity polarity): base(polarity)
		{
			p = new TEAMParameters ();
			memristance = getMemristance ();
			internalState = 0.5 * 3E-9d;
		}
		#endregion
	}
}

