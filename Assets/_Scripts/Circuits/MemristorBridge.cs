﻿using System;
using System.Diagnostics;

namespace AssemblyCSharp
{
	public partial class MemristorBridge<MemristorType>: ISynapse where MemristorType : MemristorBase
	{
		#region ISynapse implementation
		public double SynapticWeight {
			get { return synapticWeight; }
		}
		#endregion

		#region Public
		public Voltage Input {
			get { return bridgeInput; }
			set { 
				bridgeInput = value; 
				updateWeight ();
			}
		}

		#endregion

		#region private
		private Voltage bridgeInput;
		private Voltage bridgeOutput;
		private double synapticWeight;
		private MemristorType[] m = new MemristorType[4];

		private Current currentForMemristor(int memIndex, Resistance r0, Resistance r1, Resistance r2, Resistance r3) {
			//temporary assumption: differential amplifier resistance is much higher than that of the bridge
			Debug.Assert (memIndex >= 0 && memIndex <= 3);
			return memIndex <= 1 ? bridgeInput.ToCurrent (r0 + r1) : bridgeInput.ToCurrent (r2 + r3);
		}



		private void updateWeight()
		{

			Resistance r0 = new Resistance(m [0].Memristance.Value);
			Resistance r1 = new Resistance(m [1].Memristance.Value);
			Resistance r2 = new Resistance(m [2].Memristance.Value);
			Resistance r3 = new Resistance(m [3].Memristance.Value);

			for (int i = 0; i < 4; i++) {

				// provide appropriate input to memristor depending on its control type
				switch (m[i].ControlType) {
				case MemristorControlType.CurrentControlled:
					m[i].Input (currentForMemristor (i,r0,r1,r2,r3));
					break;
				case MemristorControlType.VoltageControlled:
					m[i].Input (bridgeInput);
					break;					
				}

			}

			double m1 = m [0].Memristance.Value; //negative polarity
			double m2 = m [1].Memristance.Value; //postitive polarity
			double m3 = m [2].Memristance.Value; //postitive polarity
			double m4 = m [3].Memristance.Value; //negative polarity

			synapticWeight = (m2 / (m1 + m2)) - (m4 / (m3 + m4));

			bridgeOutput = new Voltage (bridgeInput.Value * synapticWeight);
		}

		#endregion

		public MemristorBridge()
		{
			m [0] = MemristorFactory.Instantiate<MemristorType> (Polarity.negative) as MemristorType;
			m [1] = MemristorFactory.Instantiate<MemristorType> (Polarity.positive) as MemristorType;
			m [2] = MemristorFactory.Instantiate<MemristorType> (Polarity.positive) as MemristorType;
			m [3] = MemristorFactory.Instantiate<MemristorType> (Polarity.negative) as MemristorType;
		}
	}
}

