﻿using System;
using System.Diagnostics;

namespace AssemblyCSharp
{
	public partial class MemristorBridge<MemristorType>: ISynapse where MemristorType : MemristorBase
	{
		#region ISynapse implementation
		public void AttachToNeuron(INeuron neuron) {
			var al = neuron as ActiveLoad;
			Debug.Assert (al != null);
			al.AttachToNode (al.InputNodePositive, outPositiveCurrent);
			al.AttachToNode (al.InputNodeNegative, outNegativeCurrent);
		}
		#endregion

		#region public
		public double Transconductance {
			get { return transconductance; }
		}

		public Current OutPosCur {
			get { 
				outPositiveCurrent = getOutPosCur ();
				return outPositiveCurrent;
			}
		}

		public Current OutNegCur {
			get { 
				outNegativeCurrent = getOutNegCur ();
				return outPositiveCurrent;
			}		
		}

		#endregion

		#region private

		private Current outPositiveCurrent;
		private Current outNegativeCurrent;
		private readonly double transconductance;

		private Current getOutPosCur() {
			return new Current(-0.5 * transconductance * bridgeOutput.Value);
		}

		private Current getOutNegCur() {
			return new Current(0.5 * transconductance * bridgeOutput.Value);
		}
		#endregion

	}
}

