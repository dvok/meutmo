﻿using System;
using System.Collections.Generic;

namespace AssemblyCSharp
{
	public class ActiveLoad: INeuron
	{
		#region INeuron implementation



		public Current Input {
			get {
				return GetInCurrent () ;
			}
			set {
				throw new InvalidOperationException ();
			}
		}

		public Voltage Output {
			get {
				return GetOutVoltage ();
			}
		}

		#endregion

		#region public
		public List<Current> InputNodePositive {
			get { return inputNodePositive; }
		}

		public List<Current> InputNodeNegative {
			get { return inputNodeNegative; }
		}

		public void AttachToNode (List<Current> node, Current i) {
			if (!node.Contains (i))  {
				node.Add (i);
			}
		}
		#endregion

		#region private
		private Voltage output;
		private Current input;

		private List<Current> inputNodePositive = new List<Current>();
		private List<Current> inputNodeNegative = new List<Current>();


		private Voltage v_dd = new Voltage(3.3d);
		private Voltage v_ss = new Voltage(-3.3d);
		private Voltage v_th = new Voltage(0.75d);

		private Resistance r_out = new Resistance (160000d);
		private Resistance r_l = new Resistance(8000d);


		private Current InputPosSum () {
			double sum = 0d;
			foreach (var i in inputNodePositive) {
				sum += i.Value;
			}
			return new Current (sum);
		}

		private Current InputNegSum () {
			double sum = 0d;
			foreach (var i in inputNodeNegative) {
				sum += i.Value;
			}
			return new Current (sum);
		}

		private Current GetInCurrent() {
			return InputNegSum () - InputPosSum ();
		}

		private Voltage GetOutVoltage() {
			Current i = GetInCurrent ();
			if (i.Value >= ((-v_ss.Value - 2 * v_th.Value) / r_out.Value) && i.Value <= ((v_dd.Value - 2 * v_th.Value) / r_out.Value)) {
				return i.ToVoltage(r_l);
			}

			if (((v_dd.Value - 2 * v_th.Value) / r_out.Value) <= i.Value) {
				return new Voltage (v_dd.Value - 2 * v_th.Value);
			}

			if (((-v_ss.Value - 2 * v_th.Value) / r_out.Value) <= i.Value) {
				return new Voltage (-v_ss.Value - 2 * v_th.Value);
			}

			return new Voltage (0d);
		}
		#endregion
		
		public ActiveLoad ()
		{
		}
	}
}

