﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using System.Collections.Generic;
using AssemblyCSharp;

public class TestWindowFunction : MonoBehaviour {


	private List<Vector3> windOffPoints = new List<Vector3>();
	private List<Vector3> windOnPoints = new List<Vector3>();

	private VectorLine windOffLine;
	private VectorLine windOnLine;

	private TEAMParameters p;
	double w;

	// Use this for initialization
	void Start () {



	
		p = new TEAMParameters ();
		w = 0e-9d;
	}
	
	void FixedUpdate() {
		w += 1e-11d;
	}

	void Update () {
		windOffPoints.Add (new Vector3((float)w*1e9f, (float)p.F_off (w)));


		windOnPoints.Add (new Vector3((float)w*1e9f, (float)p.F_on (w)));

		Debug.Log (w*1e9f + " // " + p.F_off (w) + " // " + p.F_on (w));




		if (w > 2.4e-9d) {
			windOffLine = new VectorLine ("window function off", windOffPoints, null, 1f, LineType.Continuous);
			windOnLine = new VectorLine ("window function on", windOnPoints, null, 1f, LineType.Continuous);
			windOnLine.color = Color.red;
			windOffLine.color = Color.blue;
			windOffLine.Draw3D ();
			windOnLine.Draw3D ();
			Debug.Break ();
		}
	}
}
